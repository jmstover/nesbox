# SNESPad
NES / SNES gamepad handler for microcontrollers

### Supported MCU's
* Arduino Series
* AVR Series (WIP)
* TI MSP430 Series (WIP)


## NES Info
***
Controller Port Pins  
![NES_Controller.png](https://bitbucket.org/jmstover/nesbox/downloads/nes-controller_port.png)
***
Controller Timing  
![timing.png](https://bitbucket.org/repo/XGkzXn/images/1481977841-timing.png)
***
Power / Reset Board  
![Power/Reset](https://bitbucket.org/jmstover/nesbox/downloads/nes-power_reset_button.svg)
***
Arduino Micro NESBox Wiring  
![ArduinoMicro](https://bitbucket.org/jmstover/nesbox/downloads/nes-arduino_board_micro.svg)
***